function love.conf(t)
    t.version = "11.3"                  -- The LÖVE version this game was made for (string)

    t.window.title = "Q. W. E. R. T."         -- The window title (string)
    t.window.icon = "meta/icon.png"                 -- Filepath to an image to use as the window's icon (string)
    t.window.width = 800                -- The window width (number)
    t.window.height = 600               -- The window height (number)
    t.window.resizable = false          -- Let the window be user-resizable (boolean)
end