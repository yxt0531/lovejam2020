local dynamicIcon = require("systems/dynamicIcon")
local dynamicHalos = require("systems/dynamicHalos")

local agogoC5 = love.audio.newSource("sounds/agogo/c5.wav", "static")
local agogoD5 = love.audio.newSource("sounds/agogo/d5.wav", "static")
local agogoE5 = love.audio.newSource("sounds/agogo/e5.wav", "static")
local agogoG5 = love.audio.newSource("sounds/agogo/g5.wav", "static")
local agogoA5 = love.audio.newSource("sounds/agogo/a5.wav", "static")

local marimbaC5 = love.audio.newSource("sounds/marimba/c5.wav", "static")
local marimbaD5 = love.audio.newSource("sounds/marimba/d5.wav", "static")
local marimbaE5 = love.audio.newSource("sounds/marimba/e5.wav", "static")
local marimbaG5 = love.audio.newSource("sounds/marimba/g5.wav", "static")
local marimbaA5 = love.audio.newSource("sounds/marimba/a5.wav", "static")

local previousToneNumber = 0

local tone = {}

tone.keypressed = function(key, isHost)
    if isHost then
        if key == "q" then
            marimbaC5:stop()
            marimbaC5:play()
        elseif key == "w" then
            marimbaD5:stop()
            marimbaD5:play()
        elseif key == "e" then
            marimbaE5:stop()
            marimbaE5:play()
        elseif key == "r" then
            marimbaG5:stop()
            marimbaG5:play()
        elseif key == "t" then
            marimbaA5:stop()
            marimbaA5:play()
        end
    else
        if key == "q" then
            agogoC5:stop()
            agogoC5:play()
        elseif key == "w" then
            agogoD5:stop()
            agogoD5:play()
        elseif key == "e" then
            agogoE5:stop()
            agogoE5:play()
        elseif key == "r" then
            agogoG5:stop()
            agogoG5:play()
        elseif key == "t" then
            agogoA5:stop()
            agogoA5:play()
        end
    end
    dynamicIcon.keypressed(key)
    dynamicHalos.keypressed(key)
end

tone.update = function(dt)
    dynamicIcon.update(dt)
    dynamicHalos.update(dt)
end

tone.draw = function()
    dynamicHalos.draw()
end

tone.playRandomTone = function()
    local number = love.math.random(0, 4)
    while number == previousToneNumber do
        number = love.math.random(0, 4)
    end

    if number == 0 then
        agogoC5:stop()
        agogoC5:play()
        dynamicIcon.keypressed("q")
        dynamicHalos.keypressed("q")
    elseif number == 1 then
        agogoD5:stop()
        agogoD5:play()
        dynamicIcon.keypressed("w")
        dynamicHalos.keypressed("w")
    elseif number == 2 then
        agogoE5:stop()
        agogoE5:play()
        dynamicIcon.keypressed("e")
        dynamicHalos.keypressed("e")
    elseif number == 3 then
        agogoG5:stop()
        agogoG5:play()
        dynamicIcon.keypressed("r")
        dynamicHalos.keypressed("r")
    elseif number == 4 then
        agogoA5:stop()
        agogoA5:play()
        dynamicIcon.keypressed("t")
        dynamicHalos.keypressed("t")
    end

    previousToneNumber = number
end

--[[
tone.playHostTone = function(tone)
    if tone == 0 then
        marimbaC5:stop()
        marimbaC5:play()
        dynamicIcon.keypressed("q")
    elseif tone == 1 then
        marimbaD5:stop()
        marimbaD5:play()
        dynamicIcon.keypressed("w")
    elseif tone == 2 then
        marimbaE5:stop()
        marimbaE5:play()
        dynamicIcon.keypressed("e")
    elseif tone == 3 then
        marimbaG5:stop()
        marimbaG5:play()
        dynamicIcon.keypressed("r")
    elseif tone == 4 then
        marimbaA5:stop()
        marimbaA5:play()
        dynamicIcon.keypressed("t")
    end
end

tone.playClientTone = function(tone)
    if tone == 0 then
        agogoC5:stop()
        agogoC5:play()
        dynamicIcon.keypressed("q")
    elseif tone == 1 then
        agogoD5:stop()
        agogoD5:play()
        dynamicIcon.keypressed("w")
    elseif tone == 2 then
        agogoE5:stop()
        agogoE5:play()
        dynamicIcon.keypressed("e")
    elseif tone == 3 then
        agogoG5:stop()
        agogoG5:play()
        dynamicIcon.keypressed("r")
    elseif tone == 4 then
        agogoA5:stop()
        agogoA5:play()
        dynamicIcon.keypressed("t")
    end
end
]]

return tone