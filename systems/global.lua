local global = {}

global.scenes = {}

global.scenes.mainMenu = 1
global.scenes.game = 2
global.scenes.help = 3

global.currentScene = global.scenes.mainMenu

-- game releated settings
global.isHost = true
global.connectionIp = ""
global.connectionPort = ""
global.inputInterval = 5
-- game related settings end

-- game network status
global.isConnected = false
global.myTurn = false
global.endGame = false
global.startFlip = false
-- game network status end

return global