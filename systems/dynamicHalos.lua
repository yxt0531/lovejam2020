local sprites = require("systems/sprites")

local HALO_INIT_ALPHA = 1
local activeHalos = {}
local autoHaloTimer = 0

local dynamicHalos = {}

dynamicHalos.keypressed = function(key)
    if key == "q" then
        local newHalo = {}
        newHalo.halo = sprites.haloC5
        newHalo.x = math.random(0, 800)
        if global.currentScene == global.scenes.mainMenu or global.currentScene == global.scenes.help then
            newHalo.y = math.random(0, 600)
        else
            newHalo.y = math.random(0, 300)
        end
        newHalo.a = HALO_INIT_ALPHA
        table.insert(activeHalos, newHalo)
        -- log("new halo c5 inserted")
    elseif key == "w" then
        local newHalo = {}
        newHalo.halo = sprites.haloD5
        newHalo.x = math.random(0, 800)
        if global.currentScene == global.scenes.mainMenu or global.currentScene == global.scenes.help then
            newHalo.y = math.random(0, 600)
        else
            newHalo.y = math.random(0, 300)
        end
        newHalo.a = HALO_INIT_ALPHA
        table.insert(activeHalos, newHalo)
    elseif key == "e" then
        local newHalo = {}
        newHalo.halo = sprites.haloE5
        newHalo.x = math.random(0, 800)
        if global.currentScene == global.scenes.mainMenu or global.currentScene == global.scenes.help then
            newHalo.y = math.random(0, 600)
        else
            newHalo.y = math.random(0, 300)
        end
        newHalo.a = HALO_INIT_ALPHA
        table.insert(activeHalos, newHalo)
    elseif key == "r" then
        local newHalo = {}
        newHalo.halo = sprites.haloG5
        newHalo.x = math.random(0, 800)
        if global.currentScene == global.scenes.mainMenu or global.currentScene == global.scenes.help then
            newHalo.y = math.random(0, 600)
        else
            newHalo.y = math.random(0, 300)
        end
        newHalo.a = HALO_INIT_ALPHA
        table.insert(activeHalos, newHalo)
    elseif key == "t" then
        local newHalo = {}
        newHalo.halo = sprites.haloA5
        newHalo.x = math.random(0, 800)
        if global.currentScene == global.scenes.mainMenu or global.currentScene == global.scenes.help then
            newHalo.y = math.random(0, 600)
        else
            newHalo.y = math.random(0, 300)
        end
        newHalo.a = HALO_INIT_ALPHA
        table.insert(activeHalos, newHalo)
    end
end

dynamicHalos.update = function(dt)
    for i, h in ipairs(activeHalos) do
        if h.a > 0 then
            h.a = h.a - dt
        else
            table.remove(activeHalos, i)
            -- log("halo removed")
        end
    end

    if global.currentScene == global.scenes.help then
        -- dynamic background halo for help screen
        if autoHaloTimer <= 0 then
            autoHaloTimer = (math.random(1, 5) / 10)
            local number = love.math.random(0, 4)
            if number == 0 then
                dynamicHalos.keypressed("q")
            elseif number == 1 then
                dynamicHalos.keypressed("w")
            elseif number == 2 then
                dynamicHalos.keypressed("e")
            elseif number == 3 then
                dynamicHalos.keypressed("r")
            elseif number == 4 then
                dynamicHalos.keypressed("t")
            end
        else
            autoHaloTimer = autoHaloTimer - dt
        end
    end
end

dynamicHalos.draw = function()
    for i, h in ipairs(activeHalos) do
        love.graphics.setColor(1, 1, 1, h.a)
        love.graphics.draw(h.halo, h.x, h.y, nil, nil, nil, 256, 256)
    end
    
    love.graphics.setColor(1, 1, 1, 1)
end

return dynamicHalos