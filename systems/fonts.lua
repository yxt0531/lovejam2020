local fonts = {}

fonts.mainMenu = {}

fonts.mainMenu.label = love.graphics.newFont("fonts/IBMPlexSans-Regular.ttf", 24)
fonts.mainMenu.input = love.graphics.newFont("fonts/IBMPlexSans-Regular.ttf", 16)
fonts.mainMenu.infoTip = love.graphics.newFont("fonts/IBMPlexSans-Italic.ttf", 16)

fonts.game = {}
fonts.game.status = love.graphics.newFont("fonts/LiuJianMaoCao-Regular.ttf", 48)

return fonts