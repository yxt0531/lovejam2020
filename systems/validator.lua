isIpv4 = function(ip)
    --log("validating ip")
    if not ip then return false end

    local a, b, c, d = ip:match(
                           "^(%d%d?%d?)%.(%d%d?%d?)%.(%d%d?%d?)%.(%d%d?%d?)$")

    a = tonumber(a)
    b = tonumber(b)
    c = tonumber(c)
    d = tonumber(d)

    if not a or not b or not c or not d then return false end

    if a < 0 or 255 < a then return false end
    if b < 0 or 255 < b then return false end
    if c < 0 or 255 < c then return false end
    if d < 0 or 255 < d then return false end

    --log("ip is valid")
    return true
end

isNumber = function(num)
    --log("validating number")
    if not num then return false end

    if not tonumber(num) then return false end

    --log("number is valid")
    return true
end

isDot = function(c)
    if c == "." then return true end

    return false
end
