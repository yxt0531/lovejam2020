local icon = love.image.newImageData("meta/icon.png")

local iconC5 = love.image.newImageData("meta/iconC5.png")
local iconD5 = love.image.newImageData("meta/iconD5.png")
local iconE5 = love.image.newImageData("meta/iconE5.png")
local iconG5 = love.image.newImageData("meta/iconG5.png")
local iconA5 = love.image.newImageData("meta/iconA5.png")

local iconResetTimer = 0
local iconResetTimeOut = 0.15

local dynamicIcon = {}

dynamicIcon.keypressed = function(key)
    if key == "q" then
        love.window.setIcon(iconC5)
        iconResetTimer = iconResetTimeOut
        --log("icon changed to c5")
    elseif key == "w" then
        love.window.setIcon(iconD5)
        iconResetTimer = iconResetTimeOut
        --log("icon changed to d5")
    elseif key == "e" then
        love.window.setIcon(iconE5)
        iconResetTimer = iconResetTimeOut
        --log("icon changed to e5")
    elseif key == "r" then
        love.window.setIcon(iconG5)
        iconResetTimer = iconResetTimeOut
        --log("icon changed to g5")
    elseif key == "t" then
        love.window.setIcon(iconA5)
        iconResetTimer = iconResetTimeOut
        --log("icon changed to a5")
    end
end

dynamicIcon.update = function(dt)
    if iconResetTimer > 0 then
        iconResetTimer = iconResetTimer - dt
    elseif iconResetTimer < 0 then
        -- reset window icon
        iconResetTimer = 0
        love.window.setIcon(icon)
        --log("icon resetted")
    end
end

return dynamicIcon