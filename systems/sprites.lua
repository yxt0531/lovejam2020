local sprites = {}

sprites.noteHighlighted = love.graphics.newImage("sprites/noteHighlighted.png")

sprites.noteC5 = love.graphics.newImage("sprites/noteC5.png")
sprites.noteD5 = love.graphics.newImage("sprites/noteD5.png")
sprites.noteE5 = love.graphics.newImage("sprites/noteE5.png")
sprites.noteG5 = love.graphics.newImage("sprites/noteG5.png")
sprites.noteA5 = love.graphics.newImage("sprites/noteA5.png")

sprites.originalPosistion = {}
sprites.originalPosistion.noteC5 = {}
sprites.originalPosistion.noteD5 = {}
sprites.originalPosistion.noteE5 = {}
sprites.originalPosistion.noteG5 = {}
sprites.originalPosistion.noteA5 = {}

sprites.originalPosistion.noteC5.x = 15
sprites.originalPosistion.noteC5.y = 430
sprites.originalPosistion.noteD5.x = 170
sprites.originalPosistion.noteD5.y = 410
sprites.originalPosistion.noteE5.x = 325
sprites.originalPosistion.noteE5.y = 390
sprites.originalPosistion.noteG5.x = 480
sprites.originalPosistion.noteG5.y = 350
sprites.originalPosistion.noteA5.x = 635
sprites.originalPosistion.noteA5.y = 330

sprites.bgLines = love.graphics.newImage("sprites/bgLines.png")

sprites.haloC5 = love.graphics.newImage("sprites/haloC5.png")
sprites.haloD5 = love.graphics.newImage("sprites/haloD5.png")
sprites.haloE5 = love.graphics.newImage("sprites/haloE5.png")
sprites.haloG5 = love.graphics.newImage("sprites/haloG5.png")
sprites.haloA5 = love.graphics.newImage("sprites/haloA5.png")

return sprites

-- c5: 4fc1e8, d5: a0d568, e5: ffce54, g5: ed5564, a5: ac92eb