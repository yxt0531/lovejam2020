local fonts = require("systems/fonts")
local dynamicHalos = require("systems/dynamicHalos")

local helpText = 
"When it is your turn, remember your friend's tune and play them back before time runs out. \n\n" ..
"If your tune matches your friend's tune, then write your own tune for your friend to remember.\n\n" ..
"The person who makes his/her first mistake loses the game.\n\n\n" .. 
"          Q/W/E/R/T: Play corresponding note.\n          Backspace: Return to main menu.\n"
local help = {}

help.keypressed = function(key)
    if key == "return" or key == "escape" or key == "backspace" or key == "space" then
        global.currentScene = global.scenes.mainMenu
    end
end

help.mousepressed = function(x, y, button)
    global.currentScene = global.scenes.mainMenu
end

help.update = function(dt)
    dynamicHalos.update(dt)
end

help.draw = function()
    dynamicHalos.draw()
    love.graphics.printf(helpText, fonts.mainMenu.label, 60, 140, love.graphics.getWidth() - 80, "left")
end

return help