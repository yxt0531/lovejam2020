local enet = require "enet" -- loading enet library

local sprites = require("systems/sprites")
local tone = require("systems/tone")
local fonts = require("systems/fonts")

local game = {}

-- consts
local BOUNCE_DISTANCE = 40
local NOTE_FALLING_SPEED = 200
local CONNECTION_TIMEOUT = 10
local NOTE_PLAYBACK_DELAY = 1
local RESET_TIMEOUT = 5

local noteC5PositionX
local noteC5PositionY

local noteD5PositionX
local noteD5PositionY

local noteE5PositionX
local noteE5PositionY

local noteG5PositionX
local noteG5PositionY

local noteA5PositionX
local noteA5PositionY

local noteBounceTimeout
local noteBounceTimer

local textlabelConnectionStatus
local textlabelConnectionTip
local textlabelGameStatus

local connectionTimer

local inputNotesString
local targetNotesString
local targetNotesBuffer
local inputTimer

local maxNotes
local currentNotes

local notePlaybackTimer

local restartTimer

game.load = function()
    if global.isHost then
        log("loading game scene as host")

        host = enet.host_create("*:" .. global.connectionPort, 1)
        log("creating enet server " .. "*:" .. global.connectionPort)

        global.myTurn = true
    else
        log("loading game scene as client")

        host = enet.host_create()
        server = host:connect(global.connectionIp .. ":" .. global.connectionPort)
        log("connecting to " .. global.connectionIp .. ":" .. global.connectionPort)

        global.myTurn = false
    end

    noteC5PositionX = sprites.originalPosistion.noteC5.x
    noteC5PositionY = sprites.originalPosistion.noteC5.y
    
    noteD5PositionX = sprites.originalPosistion.noteD5.x
    noteD5PositionY = sprites.originalPosistion.noteD5.y
    
    noteE5PositionX = sprites.originalPosistion.noteE5.x
    noteE5PositionY = sprites.originalPosistion.noteE5.y
    
    noteG5PositionX = sprites.originalPosistion.noteG5.x
    noteG5PositionY = sprites.originalPosistion.noteG5.y
    
    noteA5PositionX = sprites.originalPosistion.noteA5.x
    noteA5PositionY = sprites.originalPosistion.noteA5.y

    noteBounceTimeout = 0.15
    noteBounceTimer = 0

    connectionTimer = CONNECTION_TIMEOUT

    inputNotesString = ""
    targetNotesString = ""
    targetNotesBuffer = ""
    inputTimer = 0

    maxNotes = 2
    currentNotes = 0

    textlabelGameStatus = ""

    notePlaybackTimer = 0

    global.endGame = false

    restartTimer = RESET_TIMEOUT

    log("game scene loaded")
end

game.keypressed = function(key)
    if global.isConnected == false then
        if key == "escape" then
            game.clear()
            log("connection aborted")
        end

        return
    end

    if key == "backspace" then
        log("returning to main menu")
        game.clear()
    end

    if global.endGame then
        return
    end

    if global.myTurn and currentNotes < maxNotes and #targetNotesBuffer <= 0 then
        -- process input only if its my turn
        tone.keypressed(key, global.isHost) -- create sound when pressing keys

        if key == "q" then
            noteC5PositionY = noteC5PositionY - BOUNCE_DISTANCE
            currentNotes = currentNotes + 1
            inputNotesString = inputNotesString .. "q"
            inputTimer = global.inputInterval
        elseif key == "w" then
            noteD5PositionY = noteD5PositionY - BOUNCE_DISTANCE
            currentNotes = currentNotes + 1
            inputNotesString = inputNotesString .. "w"
            inputTimer = global.inputInterval
        elseif key == "e" then
            noteE5PositionY = noteE5PositionY - BOUNCE_DISTANCE
            currentNotes = currentNotes + 1
            inputNotesString = inputNotesString .. "e"
            inputTimer = global.inputInterval
        elseif key == "r" then
            noteG5PositionY = noteG5PositionY - BOUNCE_DISTANCE
            currentNotes = currentNotes + 1
            inputNotesString = inputNotesString .. "r"
            inputTimer = global.inputInterval
        elseif key == "t" then
            noteA5PositionY = noteA5PositionY - BOUNCE_DISTANCE
            currentNotes = currentNotes + 1
            inputNotesString = inputNotesString .. "t"
            inputTimer = global.inputInterval
        end
    end
end

game.mousepressed = function(x, y, button)

end

game.mousereleased = function(x, y, button)

end

game.wheelmoved = function(x, y)

end

game.textinput = function(key)

end

game.update = function(dt)
    tone.update(dt)

    -- notes falling
    if noteC5PositionY < sprites.originalPosistion.noteC5.y then
        noteC5PositionY = noteC5PositionY + (dt * NOTE_FALLING_SPEED)
    elseif noteC5PositionY > sprites.originalPosistion.noteC5.y then
        noteC5PositionY = sprites.originalPosistion.noteC5.y
        log("note c5 position resetted")
    end

    if noteD5PositionY < sprites.originalPosistion.noteD5.y then
        noteD5PositionY = noteD5PositionY + (dt * NOTE_FALLING_SPEED)
    elseif noteD5PositionY > sprites.originalPosistion.noteD5.y then
        noteD5PositionY = sprites.originalPosistion.noteD5.y
        log("note d5 position resetted")
    end

    if noteE5PositionY < sprites.originalPosistion.noteE5.y then
        noteE5PositionY = noteE5PositionY + (dt * NOTE_FALLING_SPEED)
    elseif noteE5PositionY > sprites.originalPosistion.noteE5.y then
        noteE5PositionY = sprites.originalPosistion.noteE5.y
        log("note e5 position resetted")
    end

    if noteG5PositionY < sprites.originalPosistion.noteG5.y then
        noteG5PositionY = noteG5PositionY + (dt * NOTE_FALLING_SPEED)
    elseif noteG5PositionY > sprites.originalPosistion.noteG5.y then
        noteG5PositionY = sprites.originalPosistion.noteG5.y
        log("note g5 position resetted")
    end

    if noteA5PositionY < sprites.originalPosistion.noteA5.y then
        noteA5PositionY = noteA5PositionY + (dt * NOTE_FALLING_SPEED)
    elseif noteA5PositionY > sprites.originalPosistion.noteA5.y then
        noteA5PositionY = sprites.originalPosistion.noteA5.y
        log("note a5 position resetted")
    end
    -- notes falling end

    if global.isHost then
        -- host mode network process
        local event = host:service()
        if event and event.type == "receive" then
            -- log("message received from client:", event.data, event.peer)

            -- event.peer:send("server received")
            -- log("received notification sent client")
            if event.data == "cmd pass_turn" then
                log("turn received")
                global.myTurn = true
            elseif event.data == "cmd lost" then
                global.endGame = true
            elseif event.data:sub(1, 21) == "set targetNotesString" then
                targetNotesString = event.data:sub(25, #event.data)
                targetNotesBuffer = targetNotesString
                log("target string received: " .. targetNotesString)
            end

        elseif event and event.type == "connect" then
            log(event.peer, "connected")

            event.peer:send("connected")
            log("connection notification sent client")

            event.peer:send("set global.inputInterval = " .. tostring(global.inputInterval))

            global.isConnected = true

        elseif event and event.type == "disconnect" then
            log(event.peer, "disconnected")
            log("peer disconnected:", event.peer)

            game.clear()
        end
        -- host mode network process end
    else
        -- client mode network process
        local event = host:service()

        if event then
            if event.type == "receive" then
                if event.data == "connected" then
                    log("connected as client") 
                elseif event.data:sub(1, 24) == "set global.inputInterval" then
                    global.inputInterval = tonumber(event.data:sub(28, 28))
                    log("input interval synced with server: " .. tostring(global.inputInterval))
                    global.isConnected = true
                elseif event.data:sub(1, 21) == "set targetNotesString" then
                    targetNotesString = event.data:sub(25, #event.data)
                    targetNotesBuffer = targetNotesString
                    log("target string received: " .. targetNotesString)
                elseif event.data == "cmd pass_turn" then
                    log("turn received")
                    global.myTurn = true
                elseif event.data == "cmd lost" then
                    global.endGame = true
                end
            elseif event.type == "disconnect" then
                log("disconnected from server")
                game.clear()
            end
        end
        -- client mode network process end
    end

    -- connecting screen
    if global.isConnected == false then
        textlabelConnectionTip = "Press ESC to abort"

        if global.isHost then
            textlabelConnectionStatus = "Waiting for connection..."
        else
            textlabelConnectionStatus = "Connecting..."

            connectionTimer = connectionTimer - dt
            if connectionTimer <= 0 then
                game.clear()
                log("connection timed out")
            end
        end
    
        return
    end
    -- connecting screen end

    -- end game return
    if global.endGame then
        if global.endGame and textlabelGameStatus == "Friend's turn" then
            textlabelGameStatus = "You win"
        end

        restartTimer = restartTimer - dt

        if restartTimer <= 0 then
            game.restart()
        end
        return
    end
    -- end game return end

    if currentNotes >= maxNotes and targetNotesString == "" then
        -- pass turn to opponent
        global.myTurn = false

        currentNotes = 0
        maxNotes = maxNotes + 1

        host:get_peer(1):send("set targetNotesString = " .. inputNotesString)

        log("sending target notes string: " .. inputNotesString)

        host:get_peer(1):send("cmd pass_turn")

        inputNotesString = "" -- clear input notes string
        targetNotesString = "" -- clear target strings
        -- pass turn to opponent end
    end

    if global.myTurn then
        if #targetNotesBuffer > 0 then
            if notePlaybackTimer < NOTE_PLAYBACK_DELAY then
                notePlaybackTimer = notePlaybackTimer + dt
            else
                notePlaybackTimer = 0
                
                -- player opponent tune
                tone.keypressed(targetNotesBuffer:sub(1, 1), not global.isHost)

                -- bounce notes
                if targetNotesBuffer:sub(1, 1) == "q" then
                    noteC5PositionY = noteC5PositionY - BOUNCE_DISTANCE
                elseif targetNotesBuffer:sub(1, 1) == "w" then
                    noteD5PositionY = noteD5PositionY - BOUNCE_DISTANCE
                elseif targetNotesBuffer:sub(1, 1) == "e" then
                    noteE5PositionY = noteE5PositionY - BOUNCE_DISTANCE
                elseif targetNotesBuffer:sub(1, 1) == "r" then
                    noteG5PositionY = noteG5PositionY - BOUNCE_DISTANCE
                elseif targetNotesBuffer:sub(1, 1) == "t" then
                    noteA5PositionY = noteA5PositionY - BOUNCE_DISTANCE
                end
                -- bounce notes end
                
                log("playing note: " .. targetNotesBuffer:sub(1, 1))

                if #targetNotesBuffer == 1 then
                    targetNotesBuffer = ""

                    inputTimer = tostring(global.inputInterval)
                else
                    targetNotesBuffer = targetNotesBuffer:sub(2, #targetNotesBuffer)
                end
            end
            textlabelGameStatus = "Remember your friend's tune"
            
        elseif #targetNotesString == 0 then
            if currentNotes == 0 then
                textlabelGameStatus = "Write your tune to your friend"
            elseif currentNotes < maxNotes then
                textlabelGameStatus = "Remaining notes: " .. tostring(maxNotes - currentNotes)
            end
        elseif #targetNotesString > 0 and #targetNotesBuffer == 0 then
            inputTimer = inputTimer - dt
            if inputTimer <= 0 then
                -- timeout game over
                if not global.endGame then
                    host:get_peer(1):send("cmd lost")
                    textlabelGameStatus = "You ran out of time"
                    global.endGame = true
                    return
                end
            end

            if currentNotes == 0 then
                textlabelGameStatus = "Your turn, start before time runs out: " .. tostring(inputTimer):sub(1, 1) .. "s"
            elseif currentNotes < #targetNotesString then
                textlabelGameStatus = "Remaining notes: " .. tostring(#targetNotesString - currentNotes) .. ", t = " .. tostring(inputTimer):sub(1, 1) .. "s"
            elseif currentNotes == #targetNotesString then
                if inputNotesString == targetNotesString then
                    targetNotesString = "" -- win turn
                    currentNotes = 0
                    inputNotesString = ""
                else
                    if not global.endGame then
                        host:get_peer(1):send("cmd lost")
                        textlabelGameStatus = "Your tune doesn't match"
                        global.endGame = true
                    end
                end
            end
        end
    else
        textlabelGameStatus = "Friend's turn"
    end
end

game.draw = function()
    if global.isConnected == false then
        love.graphics.printf(textlabelConnectionStatus, fonts.mainMenu.label, love.graphics.getWidth()/2, 300, love.graphics.getWidth(), nil, nil, nil, nil, fonts.mainMenu.label:getWidth(textlabelConnectionStatus)/2, fonts.mainMenu.label:getHeight())
        love.graphics.printf(textlabelConnectionTip, fonts.mainMenu.input, love.graphics.getWidth()/2, 340, love.graphics.getWidth(), nil, nil, nil, nil, fonts.mainMenu.input:getWidth(textlabelConnectionTip)/2, fonts.mainMenu.input:getHeight())
        return
    end

    love.graphics.draw(sprites.bgLines)

    tone.draw()

    love.graphics.printf(textlabelGameStatus, fonts.game.status, love.graphics.getWidth()/2, 160, love.graphics.getWidth(), nil, nil, nil, nil, fonts.game.status:getWidth(textlabelGameStatus)/2, fonts.game.status:getHeight())

    -- determine if notes are highlighted
    if noteC5PositionY < sprites.originalPosistion.noteC5.y then
        love.graphics.draw(sprites.noteHighlighted, noteC5PositionX, noteC5PositionY)
    else
        love.graphics.draw(sprites.noteC5, noteC5PositionX, noteC5PositionY)
    end

    if noteD5PositionY < sprites.originalPosistion.noteD5.y then
        love.graphics.draw(sprites.noteHighlighted, noteD5PositionX, noteD5PositionY)
    else
        love.graphics.draw(sprites.noteD5, noteD5PositionX, noteD5PositionY)
    end

    if noteE5PositionY < sprites.originalPosistion.noteE5.y then
        love.graphics.draw(sprites.noteHighlighted, noteE5PositionX, noteE5PositionY)
    else
        love.graphics.draw(sprites.noteE5, noteE5PositionX, noteE5PositionY)
    end

    if noteG5PositionY < sprites.originalPosistion.noteG5.y then
        love.graphics.draw(sprites.noteHighlighted, noteG5PositionX, noteG5PositionY)
    else
        love.graphics.draw(sprites.noteG5, noteG5PositionX, noteG5PositionY)
    end

    if noteA5PositionY < sprites.originalPosistion.noteA5.y then
        love.graphics.draw(sprites.noteHighlighted, noteA5PositionX, noteA5PositionY)
    else
        love.graphics.draw(sprites.noteA5, noteA5PositionX, noteA5PositionY)
    end
    -- determine if notes are highlighted end
end

game.quit = function()
    if global.isConnected then
        game.clear()
    end
end

game.clear = function()
    if global.isConnected then
        host:get_peer(1):disconnect()
        host:flush()
    end

    global.currentScene = global.scenes.mainMenu
    global.isConnected = false
    host:destroy()
    
    log("game scene cleared")

    global.currentScene = global.scenes.mainMenu
end

game.restart = function()
    if global.endGame then
        if global.startFlip then
            global.startFlip = false
        else
            global.startFlip = true
        end

        log("restarting current game")
        if global.isHost then
            if global.startFlip then
                global.myTurn = false
            else
                global.myTurn = true
            end
            
        else
            if global.startFlip then
                global.myTurn = true
            else
                global.myTurn = false
            end
            
        end
    
        noteC5PositionX = sprites.originalPosistion.noteC5.x
        noteC5PositionY = sprites.originalPosistion.noteC5.y
        
        noteD5PositionX = sprites.originalPosistion.noteD5.x
        noteD5PositionY = sprites.originalPosistion.noteD5.y
        
        noteE5PositionX = sprites.originalPosistion.noteE5.x
        noteE5PositionY = sprites.originalPosistion.noteE5.y
        
        noteG5PositionX = sprites.originalPosistion.noteG5.x
        noteG5PositionY = sprites.originalPosistion.noteG5.y
        
        noteA5PositionX = sprites.originalPosistion.noteA5.x
        noteA5PositionY = sprites.originalPosistion.noteA5.y
    
        noteBounceTimeout = 0.15
        noteBounceTimer = 0
    
        connectionTimer = CONNECTION_TIMEOUT
    
        inputNotesString = ""
        targetNotesString = ""
        targetNotesBuffer = ""
        inputTimer = 0
    
        maxNotes = 2
        currentNotes = 0
    
        textlabelGameStatus = ""
    
        notePlaybackTimer = 0
    
        global.endGame = false
    
        restartTimer = RESET_TIMEOUT
    end
end

return game