gui = require('libraries/Gspot')

require("systems/validator")
local tone = require("systems/tone")
local fonts = require("systems/fonts")

local mainMenu = {}

-- consts
local MAIN_MENU, WAITING_FOR_PORT_AS_CLIENT, WAITING_FOR_PORT_AS_HOST, WAITING_FOR_INTERVAL = 0, 1, 2, 3 -- state for main menu scene

-- text labels
local textlabelEnterIpOrPort
local textlabelInfoTip

-- user inputs
local inputIp
local inputPort
local inputInterval

-- current state
local state

-- buttons
local buttonLeft
local buttonRight

mainMenu.load = function()
    textlabelEnterIpOrPort = "Please enter the IP address:"
    textlabelInfoTip = ""

    inputIp = ""
    inputPort = "16305"
    inputInterval = "5"

    state = MAIN_MENU -- default state to main menu

    buttonLeft = gui:button('Connect to Player', {x = 260, y = 370, w = 100, h = gui.style.unit}) -- a button(label, pos, optional parent) gui.style.unit is a standard gui unit (default 16), used to keep the interface tidy
    buttonLeft.click = function(this) -- set element:click() to make it respond to gui's click event
        mainMenu.leftButtonClicked()
    end
    -- buttonLeft.pos.x = 500
    
    buttonRight = gui:button('Start as Host', {x = 440, y = 370, w = 100, h = gui.style.unit}) -- a button(label, pos, optional parent) gui.style.unit is a standard gui unit (default 16), used to keep the interface tidy
    buttonRight.click = function(this) -- set element:click() to make it respond to gui's click event
        mainMenu.rightButtonClicked()
    end
    
    ButtonHelp = gui:button('Help', {x = 720, y = 560, w = 60, h = gui.style.unit}) -- a button(label, pos, optional parent) gui.style.unit is a standard gui unit (default 16), used to keep the interface tidy
    ButtonHelp.click = function(this) -- set element:click() to make it respond to gui's click event
        log("help button clicked")
        global.currentScene = global.scenes.help
    end

    log("main menu scene loaded")
end

mainMenu.keypressed = function(key)
    --gspot related events
	if gui.focus then
		gui:keypress(key) -- only sending input to the gui if we're not using it for something else
	else
		if key == 'return' then -- binding enter key to input focus
			-- input:focus()
		elseif key == 'f1' then -- toggle show-hider
			-- if showhider.display then showhider:hide() else showhider:show() end
		else
			-- gui:feedback(key) -- why not
		end
    end
    --gspot related events end

    tone.keypressed(key, true) -- create sound when pressing keys
    
    if #key >= 3 and key:sub(1,2) == "kp" then
        if key == "kpenter" then
            key = "return"
        else
            key = key:sub(3,3)
        end
    end

    -- read input
    if state == MAIN_MENU then
        if (isNumber(key) or isDot(key)) and #inputIp < 15 then
            inputIp = tostring(inputIp .. key)
            tone:playRandomTone()
        elseif key == "backspace" and #inputIp > 0 then
            inputIp = tostring(inputIp:sub(0, #inputIp - 1))
            tone:playRandomTone()
        end

        if key == "return" then
            if isIpv4(inputIp) then
                mainMenu.leftButtonClicked()
            else
                mainMenu.rightButtonClicked()
            end
        elseif key == "escape" then
            -- do nothing when escape key hit
        end
    elseif state == WAITING_FOR_PORT_AS_CLIENT then
        if isNumber(key) and #inputPort < 5 then
            inputPort = tostring(inputPort .. key)
            tone:playRandomTone()
        elseif key == "backspace" and #inputPort > 0 then
            inputPort = tostring(inputPort:sub(0, #inputPort - 1))
            tone:playRandomTone()
        end

        if key == "return" then
            mainMenu.leftButtonClicked()
        elseif key == "escape" then
            mainMenu.rightButtonClicked()
        end
    elseif state == WAITING_FOR_PORT_AS_HOST then
        if isNumber(key) and #inputPort < 5 then
            inputPort = tostring(inputPort .. key)
            tone:playRandomTone()
        elseif key == "backspace" and #inputPort > 0 then
            inputPort = tostring(inputPort:sub(0, #inputPort - 1))
            tone:playRandomTone()
        end

        if key == "return" then
            mainMenu.leftButtonClicked()
        elseif key == "escape" then
            mainMenu.rightButtonClicked()
        end
    elseif state == WAITING_FOR_INTERVAL then
        if isNumber(key) and tonumber(key) > 0 then
            inputInterval = tostring(key)
            tone:playRandomTone()
        end

        if key == "return" then
            mainMenu.leftButtonClicked()
        elseif key == "escape" then
            mainMenu.rightButtonClicked()
        end
    end
    -- read input end
end

mainMenu.mousepressed = function(x, y, button)
    --gspot related events
    gui:mousepress(x, y, button) -- pretty sure you want to register mouse events
    --gspot related events end
end

mainMenu.mousereleased = function(x, y, button)
    --gspot related events
    gui:mouserelease(x, y, button)
    --gspot related events end
end

mainMenu.wheelmoved = function(x, y)
    --gspot related events
    gui:mousewheel(x, y)
    --gspot related events end
end

mainMenu.textinput = function(key)
    --gspot related events
	if gui.focus then
		gui:textinput(key) -- only sending input to the gui if we're not using it for something else
    end
    --gspot related events end
end

mainMenu.update = function(dt)
    gui:update(dt) --update gspot gui

    tone.update(dt)

    --update static element
    if state == MAIN_MENU then
        textlabelEnterIpOrPort = "Please enter the IP address:"
        buttonLeft.label = "Connect to Player"
        buttonRight.label = "Start as Host"
    elseif state == WAITING_FOR_PORT_AS_CLIENT then
        textlabelEnterIpOrPort = "Please enter the port to host:"
        buttonLeft.label = "Connect"
        buttonRight.label = "Back"
    elseif state == WAITING_FOR_PORT_AS_HOST then
        textlabelEnterIpOrPort = "Please enter the port as host:"
        buttonLeft.label = "Confirm"
        buttonRight.label = "Back"
    elseif state == WAITING_FOR_INTERVAL then
        textlabelEnterIpOrPort = "Please enter the input interval between notes (1 - 9):"
        buttonLeft.label = "Start"
        buttonRight.label = "Back"
    end
end

mainMenu.draw = function()
    tone.draw()

    gui:draw() --draw gspot gui

    love.graphics.printf(textlabelEnterIpOrPort, fonts.mainMenu.label, love.graphics.getWidth()/2, 300, love.graphics.getWidth(), nil, nil, nil, nil, fonts.mainMenu.label:getWidth(textlabelEnterIpOrPort)/2, fonts.mainMenu.label:getHeight())

    if state == MAIN_MENU then
        love.graphics.printf(inputIp, fonts.mainMenu.input, love.graphics.getWidth()/2, 340, love.graphics.getWidth(), nil, nil, nil, nil, fonts.mainMenu.input:getWidth(inputIp)/2, fonts.mainMenu.input:getHeight())
    elseif state == WAITING_FOR_PORT_AS_CLIENT then
        love.graphics.printf(inputPort, fonts.mainMenu.input, love.graphics.getWidth()/2, 340, love.graphics.getWidth(), nil, nil, nil, nil, fonts.mainMenu.input:getWidth(inputPort)/2, fonts.mainMenu.input:getHeight())
    elseif state == WAITING_FOR_PORT_AS_HOST then
        love.graphics.printf(inputPort, fonts.mainMenu.input, love.graphics.getWidth()/2, 340, love.graphics.getWidth(), nil, nil, nil, nil, fonts.mainMenu.input:getWidth(inputPort)/2, fonts.mainMenu.input:getHeight())
    elseif state == WAITING_FOR_INTERVAL then
        love.graphics.printf(inputInterval .. " s", fonts.mainMenu.input, love.graphics.getWidth()/2, 340, love.graphics.getWidth(), nil, nil, nil, nil, fonts.mainMenu.input:getWidth(inputInterval)/2, fonts.mainMenu.input:getHeight())
    end

    love.graphics.printf(textlabelInfoTip, fonts.mainMenu.infoTip, 20, 560, 1000)
end

mainMenu.quit = function()

end

mainMenu.leftButtonClicked = function()
    log("left button clicked")
    tone:playRandomTone()
    if state == MAIN_MENU then
        if not isIpv4(inputIp) then
            textlabelInfoTip = "The entered IP address is invalid, you need a valid address to connect to other player."
        else
            state = WAITING_FOR_PORT_AS_CLIENT
            textlabelInfoTip = ""
        end
    elseif state == WAITING_FOR_PORT_AS_CLIENT then
        if not isNumber(inputPort) or tonumber(inputPort) < 1024 or tonumber(inputPort) > 65535 then
            textlabelInfoTip = "The entered port is invalid, port needs to between 1024 and 65535."
        else
            global.isHost = false
            global.connectionIp = inputIp
            global.connectionPort = inputPort

            global.currentScene = global.scenes.game
            log("game started as client, connecting to address: " .. global.connectionIp .. ":" .. global.connectionPort)

            textlabelInfoTip = "Disconnected."
        end
    elseif state == WAITING_FOR_PORT_AS_HOST then
        if not isNumber(inputPort) or tonumber(inputPort) < 1024 or tonumber(inputPort) > 65535 then
            textlabelInfoTip = "The entered port is invalid, port needs to between 1024 and 65535."
        else
            state = WAITING_FOR_INTERVAL
        end
    elseif state == WAITING_FOR_INTERVAL then
        global.isHost = true
        global.connectionPort = inputPort
        global.inputInterval = tonumber(inputInterval)

        global.currentScene = global.scenes.game
        log("game started as host, listening to port: " .. global.connectionPort)
        log("input interval is set to " .. global.inputInterval)

        textlabelInfoTip = "Connection closed."
    end
end

mainMenu.rightButtonClicked = function()
    log("right button clicked")
    tone:playRandomTone()
    if state == MAIN_MENU then
        state = WAITING_FOR_PORT_AS_HOST
        textlabelInfoTip = ""
    elseif state == WAITING_FOR_PORT_AS_CLIENT then
        state = MAIN_MENU
        textlabelInfoTip = ""
    elseif state == WAITING_FOR_PORT_AS_HOST then
        state = MAIN_MENU
        textlabelInfoTip = ""
    elseif state == WAITING_FOR_INTERVAL then
        state = WAITING_FOR_PORT_AS_HOST
        textlabelInfoTip = ""
    end
end

return mainMenu