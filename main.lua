require("systems/log")
global = require("systems/global")

scenes = {}

scenes.mainMenu = require("scenes/mainMenu")
scenes.game = require("scenes/game")
scenes.help = require("scenes/help")

function love.load()
    scenes.mainMenu.load()
    --scenes.game.load()

    localPreviousScene = global.currentScene
end

function love.keypressed(key, scancode, isrepeat)
    if global.currentScene == global.scenes.mainMenu then
        scenes.mainMenu.keypressed(key)
    elseif global.currentScene == global.scenes.game then
        scenes.game.keypressed(key)
    elseif global.currentScene == global.scenes.help then
        scenes.help.keypressed(key)
    end
end

function love.mousepressed(x, y, button)
    if global.currentScene == global.scenes.mainMenu then
        scenes.mainMenu.mousepressed(x, y, button)
    elseif global.currentScene == global.scenes.game then
        scenes.game.mousepressed(x, y, button)
    elseif global.currentScene == global.scenes.help then
        scenes.help.mousepressed(x, y, button)
    end
end

function love.mousereleased(x, y, button)
    if global.currentScene == global.scenes.mainMenu then
        scenes.mainMenu.mousereleased(x, y, button)
    elseif global.currentScene == global.scenes.game then
        scenes.game.mousereleased(x, y, button)
    end
end

function love.wheelmoved(x, y)
    if global.currentScene == global.scenes.mainMenu then
        scenes.mainMenu.wheelmoved(x, y)
    elseif global.currentScene == global.scenes.game then
        scenes.game.wheelmoved(x, y)
    end
end

function love.textinput(key)
    if global.currentScene == global.scenes.mainMenu then
        scenes.mainMenu.textinput(key)
    elseif global.currentScene == global.scenes.game then
        scenes.game.textinput(key)
    end
end

function love.update(dt)
    if global.currentScene ~= localPreviousScene then
        -- scene changed
        log("scene change detected")
        if global.currentScene == global.scenes.mainMenu then
            -- main scene changed to main menu
        elseif global.currentScene == global.scenes.game then
            scenes.game.load()
        end

        localPreviousScene = global.currentScene
    end

    if global.currentScene == global.scenes.mainMenu then
        scenes.mainMenu.update(dt)
    elseif global.currentScene == global.scenes.game then
        scenes.game.update(dt)
    elseif global.currentScene == global.scenes.help then
        scenes.help.update(dt)
    end
end

function love.draw()
    if global.currentScene == global.scenes.mainMenu then
        scenes.mainMenu.draw()
    elseif global.currentScene == global.scenes.game then
        scenes.game.draw()
    elseif global.currentScene == global.scenes.help then
        scenes.help.draw()
    end
end

function love.quit()
    log("exiting game")
    if global.currentScene == global.scenes.mainMenu then
        scenes.mainMenu.quit()
    elseif global.currentScene == global.scenes.game then
        scenes.game.quit()
    end
end